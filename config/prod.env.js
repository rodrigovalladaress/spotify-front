'use strict'
module.exports = {
  NODE_ENV: '"production"',
  SPOTIFY: {
    CLIENT_ID: '"f4f03e02ab094b0383da99915821e82f"',
    REDIRECT_URI: '"http://localhost:8080/authorize_redirected/"',
    ENDPOINTS: {
      AUTHORIZE: '"https://accounts.spotify.com/authorize/"',
      TOKEN: '"https://accounts.spotify.com/api/token/"',
      TOP_ARTISTS: '"	https://api.spotify.com/v1/me/top/artists"'
    },
  }
}
