# spotify-front-test

Shows a list of photos of the user's most listened artists. They are ordered from most listened to least listened, and the images are resized to match their general popularity on Spotify.

## Technical info
- The project is based on [Vue Webpack template](http://vuejs-templates.github.io/webpack/). This template uses Webpack, Eslint and Unit testing configuration
- For the authorization process, I've used the [Implicit Grant Flow](https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow), as the app is frontend based and the other flows required a backend to store the client secret.
- Apart from the template's base libraries, I've used FontAwesome and Sass.

![picture](sample.PNG)

## Build Setup (from vue-template)

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
