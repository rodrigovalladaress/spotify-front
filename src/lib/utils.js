const getFrom = (str, regex, property) => {
  const v = regex.exec(str);
  return v && v.length > 1 ? v[1] : null;
};
// Get properties from the hash of Spotify authorize endpoint.
const getFromHash = (hash, property) => {
  const regex = new RegExp(`.*${property}=(.+?)(?:&.*|$)`);
  return getFrom(hash, regex, property);
};

const getFromStorage = property => {
  return window.localStorage.getItem(property);
};

const addToStorage = (property, value) => {
  window.localStorage.setItem(property, value);
};

export default {
  getFromHash,
  getFromStorage,
  addToStorage
};
