import { mapMutations } from "vuex";

import utils from "@/lib/utils";

export default {
  methods: {
    ...mapMutations(["updateToken", "updateExpiringDate"]),

    updateStoreFromStorage() {
      const token = utils.getFromStorage("token");
      const expiringDate = utils.getFromStorage("expiringDate");
      this.updateToken(token);
      this.updateExpiringDate(expiringDate);
    }
  }
};
