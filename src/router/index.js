import Vue from "vue";
import Router from "vue-router";

import Authorization from "@/components/Authorization/Authorization.vue";
import Home from "@/components/Home/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/authorize",
      name: "Authorization",
      component: Authorization
    },
    {
      path: "/authorize_redirected",
      name: "AuthorizeRedirected",
      component: Authorization,
      props: {
        redirected: true
      }
    }
  ]
});
