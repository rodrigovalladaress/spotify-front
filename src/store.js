import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    token: "",
    expiringDate: new Date()
  },

  getters: {
    token: state => {
      return state.token;
    },
    expiringDate: state => {
      return state.expiringDate;
    }
  },

  mutations: {
    updateToken: (state, token) => {
      state.token = token;
    },
    updateExpiringDate: (state, expiringDate) => {
      state.expiringDate = expiringDate;
    }
  }
});

export default store;
