// Redirects the user to the Spotify's app authorization and stores the token
// and the expiring date of the token on Vuex.
import { stringify } from "querystring";
import utils from "@/lib/utils";
import { mapMutations } from "vuex";

import base from "@/lib/mixins/base";

export default {
  props: {
    redirected: { type: Boolean }
  },

  mixins: [base],

  data() {
    return {
      error: ""
    };
  },

  methods: {
    ...mapMutations(["updateToken", "updateExpiringDate"]),

    updateStorageAndStore(token, expiringDate) {
      this.updateToken(token);
      this.updateExpiringDate(expiringDate);
      utils.addToStorage("token", token);
      utils.addToStorage("expiringDate", expiringDate.getTime());
    },

    // Redirects the user to Spotify's app authorization.
    authorize() {
      const SPOTIFY_CONF = process.env.SPOTIFY;
      const urlObject = {
        client_id: SPOTIFY_CONF.CLIENT_ID,
        response_type: "token",
        redirect_uri: SPOTIFY_CONF.REDIRECT_URI,
        scope: "user-top-read"
      };
      const url = `${SPOTIFY_CONF.ENDPOINTS.AUTHORIZE}?${stringify(urlObject)}`;
      window.location.href = url;
    }
  },

  created() {
    // The first time, this.redirected is false. When the user is redirected to
    // this page after Spotify's app authorization, this.redirected is true (see
    // router/index.js).
    if (!this.redirected) {
      this.authorize();
    } else {
      const { error } = this.$route.query;
      this.error = error;

      if (!error) {
        // Hash stores the response of Spotify.
        const hash = this.$route.hash;
        const token = utils.getFromHash(hash, "access_token");
        const expiresIn = utils.getFromHash(hash, "expires_in");
        const expiresInMilliseconds = expiresIn * 1000;
        const now = new Date();
        const expiringDate = new Date(now.getTime() + expiresInMilliseconds);

        this.updateStorageAndStore(token, expiringDate);

        this.$router.push("/");
      }
    }
  }
};
