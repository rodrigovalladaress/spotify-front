// Shows a list of photos of the user's most listened artists. They are ordered
// from most listened to least listened, and the images are resized to match
// their general popularity on Spotify.
import base from "@/lib/mixins/base";

import { mapGetters } from "vuex";

const TOTAL_ARTISTS = 10;

export default {
  mixins: [base],

  data() {
    return {
      topArtistsRes: null,
      error: null,
      loading: false,
      topPopularity: null,
      leastPopularity: null,
      artistMaxHeight: null,
      artistMaxWidth: null
    };
  },

  computed: {
    ...mapGetters(["token", "expiringDate"]),

    // Creates the array of artists to render.
    topArtists() {
      const res = this.topArtistsRes;
      if (res) {
        let topPopularity = -Infinity;
        let leastPopularity = Infinity;
        const topArtists = [];
        let i = 1;

        for (const item of res.items) {
          const { name, popularity, images } = item;
          const image = images.length > 0 ? images[0] : null;

          if (topPopularity < popularity) {
            topPopularity = popularity;
          }
          if (leastPopularity > popularity) {
            leastPopularity = popularity;
          }

          topArtists.push({
            title: `${i} ${name}`,
            popularity,
            image: image ? image.url : null
          });
          i++;
        }

        const artistMaxWidth = Math.floor(100 / (TOTAL_ARTISTS / 2));
        const artistMaxHeight = Math.floor(100 / 2);

        for (const item of topArtists) {
          const percent = item.popularity / topPopularity;
          item.style = {
            width: `${Math.floor(artistMaxWidth * percent)}vw`,
            height: `${Math.floor(artistMaxHeight * percent)}vh`,
            "background-image": `url(${item.image})`
          };
        }

        this.topPopularity = topPopularity;
        this.leastPopularity = leastPopularity;
        this.artistMaxWidth = artistMaxWidth;
        this.artistMaxHeight = artistMaxHeight;

        return topArtists;
      } else {
        return [];
      }
    }
  },

  methods: {
    // Redirects to authorization page.
    authorize() {
      this.$store.state.expiringDate = new Date();
      this.$router.push("/authorize");
    },

    // Gets top artists info from Spotify.
    async getTopArtists() {
      this.loading = true;

      const SPOTIFY_CONF = process.env.SPOTIFY;
      const url = `${
        SPOTIFY_CONF.ENDPOINTS.TOP_ARTISTS
      }?limit=${TOTAL_ARTISTS}`;

      let res = null;
      try {
        res = await fetch(url, {
          method: "GET",
          mode: "cors",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${this.$store.state.token}`
          }
        });

        if (res.ok && res.status === 200) {
          this.topArtistsRes = await res.json();
        } else {
          if (res.body) {
            this.error = await res.json();
          } else {
            this.error = `status: ${res.status} type: ${res.type}`;
          }
        }
      } catch (err) {
        console.error(err);
      } finally {
        this.loading = false;
      }
    }
  },

  // Checks if the token has expired in order to redirect to authorization page.
  created() {
    this.updateStoreFromStorage();
    const now = new Date();
    const tokenExpiringDate = this.expiringDate;

    if (tokenExpiringDate <= now) {
      this.$router.push("/authorize");
    } else {
      this.getTopArtists();
    }
  }
};
